module modernc.org/plot

go 1.20

require (
	modernc.org/golex v1.1.0
	modernc.org/mathutil v1.6.0
	modernc.org/strutil v1.2.0
	modernc.org/xc v1.1.0
)

require github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
