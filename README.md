# plot

Package plot wraps Gnuplot, the portable command-line driven graphing utility
for Linux, OS/2, MS Windows, OSX, VMS, and many other platforms.

Installation

    $ go get modernc.org/plot

Documentation: [godoc.org/modernc.org/plot](http://godoc.org/modernc.org/plot)
